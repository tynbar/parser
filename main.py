import csv
import os
import json

parameters = []
count_error = 0


def generate_uid(time):
    if (time != 0):
        split_time = time.split(":")
        tmp = split_time[2].split(",")
        return split_time[0] + split_time[1] + tmp[0] + tmp[1]
    else:
        global count_error
        count_error += 1
        return "error" + str(count_error)


def clear_new_line_char(lines):
    lines = [elem for elem in lines if elem != '\n']

    for i in range(len(lines)):
        lines[i] = lines[i].replace('\n', '')

    return lines


def separate_time(line):
    output = []
    for i in range(len(line)):
        output.append(line[i].split('-'))
        if output[i][0].startswith('ERROR'):
            output[i].append(output[i][0])
            output[i][0] = 0
        if output[i][1].startswith(' '):
            output[i][1] = output[i][1][1:]
        if output[i][1].endswith(' '):
            output[i][1] = output[i][1][:-1]
    return output


def check_end_of_block(block_size, i):
    block_start_id = i - block_size
    while i >= block_start_id:

        if parameters[i]['block_end'] == 'END_OF_FILE':
            parameters[i]['block_end'] = 'BLOCK_END_1'
        i -= 1


def group_param(lines, conf):
    block = False

    for row in lines:
        line_uid = generate_uid(row[0])

        if conf["block_end"].split("|")[0] in row[1] or conf["block_end"].split("|")[1] in row[1]:
            block = False
            block_uid = ""
            block_time = ""

        if conf["block_start"] in row[1]:
            block = True
            block_uid = generate_uid(row[0])
            block_time = row[0]

        if conf["block_start"] == "":
            block = True

        if row[1].startswith(conf["line_start"]):
            if row[1].endswith(conf["line_end"].split("|")[0]) or row[1].endswith(conf["line_end"].split("|")[1]):
                split_line = row[1].split(" ")
                for line_section in split_line:
                    if conf["param_name"] in line_section:
                        if block is True:
                            if conf["param_bracket"] != "":
                                param = line_section.replace(conf["param_name"], "")[1:]
                                call_param(param, conf["param_name"], line_uid, row[0], conf["line_start"], conf["line_end"], block_uid, block_time, conf["block_start"], conf["block_end"])
                            else:
                                call_param(row[1], conf["param_name"], line_uid, row[0], conf["line_start"],conf["line_end"])
                                break

def call_param(param_val="", param_name="", line_uid="", line_time="", line_start="", line_end="", block_uid="",
               block_time="", block_start="", block_end=""):
    obj = {
        "param_val": param_val,
        "param_name": param_name,
        "line_uid": line_uid,
        "line_time": line_time,
        "line_start": line_start,
        "line_end": line_end,
        "block_uid": block_uid,
        "block_time": block_time,
        "block_start": block_start,
        "block_end": block_end,

    }

    parameters.append(obj)


if __name__ == "__main__":
    file_list = os.listdir("in")
    with open('config.json') as f:
        json_conf = json.load(f)

    f = open("in/" + file_list[0])
    lines = f.readlines()
    clear_lines = clear_new_line_char(lines)
    separate_lines = separate_time((clear_lines))

    for i in range(len(json_conf)):
        group_param(separate_lines, json_conf[i])

    with open("out/" + file_list[0] + "_out.csv", 'w') as csvfile:
        fieldnames = ['param_val', 'param_name', 'line_uid', 'line_time', 'line_start', 'line_end', 'block_uid',
                      'block_time', 'block_start', 'block_end', ]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()
        for row in parameters:
            writer.writerow(row)

    print("Writing complete")